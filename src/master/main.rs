#![feature(getpid)]

extern crate x11;
extern crate toml;
extern crate regex;

mod x11if;
mod conditionlogic;
mod configparser;
mod discordinterface;

use std::time::Duration;
use std::thread::sleep;

fn main() {
  let mut di = discordinterface::DiscordInterface::new();
  let config = configparser::from_file("conf/intellij.toml");
  let dur = Duration::from_secs(5);
  loop {
    let windows = x11if::scan();
    let res = config.entry_matches_all(&windows);
    di.update(res);
    sleep(dur);
  }
}
