use conditionlogic::Rule;
use std::process::{self, Command, Child};
use std::fs::{copy, DirBuilder};

pub struct DiscordInterface {
  process: Option<Process>
}

struct Process {
  pub name: String,
  pub c: Child
}

impl DiscordInterface {
  pub fn new() -> DiscordInterface {
    DiscordInterface { process: None }
  }

  pub fn update(&mut self, new: Option<&Rule>) {
    match new {
      Some(r) => self.enable(r),
      None => self.kill_process(None)
    }
  }

  fn enable(&mut self, rule: &Rule) {
    let wrap = Some(rule);
    if self.differs(wrap) {
      self.kill_process(wrap);
      self.spawn_process(&rule.name);
    }
  }

  fn kill_process(&mut self, new: Option<&Rule>) {
    if self.differs(new) && self.process.is_some() {
      println!("Kill process {}", self.process.as_ref().unwrap().name);
      self.process.as_mut().unwrap().c.kill();
      self.process = None;
    }
  }

  fn differs(&self, new: Option<&Rule>) -> bool {
    let has_process = self.process.is_some();

    if has_process {
      match new {
        Some(c) => c.name != self.process.as_ref().unwrap().name,
        None => true
      }
    } else {
      new.is_some()
    }
  }

  fn spawn_process(&mut self, name: &str) {
    println!("Spawn process {}", name);
    DirBuilder::new()
      .recursive(true)
      .create(format!("Games/{}", name));

    let binary = format!("Games/{0}/{0}", name);
    copy("bin/slave", &binary);
    let c = Command::new(&binary)
      .arg(process::id().to_string())
      .spawn()
      .expect("Unable to spawn process!");
    self.process = Some(Process::new(name, c));
  }
}

impl Process {
  pub fn new(name: &str, c: Child) -> Process {
    Process { name: name.to_string(), c }
  }
}